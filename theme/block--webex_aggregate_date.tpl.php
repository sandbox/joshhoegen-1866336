<?php
/**
 * @file
 * Template.
 * 
 * @see function bundle_output().
 */
?>

<?php

foreach ($variables['webex_aggregate']['data'] as $s_category_k => $s_category) :
  if (!empty($s_category_k)) :
?><h3><?php
    print t('@val', array('@val' => $s_category_k));
?></h3><?php
  endif;
?>
    <?php
  foreach ($s_category as $s_date => $s_date_object) :
?>
        <h4><?php
    print format_date(strtotime($s_date));
?></h4>
        <ul>
        <?php
    foreach ($s_date_object as $s_title => $s_group_data) :
?>
            <li>
              <?php
      print t('@val', array('@val' => $s_group_data->sessionNameOutput));
?>
              <div><?php
      print t('@val', array('@val' => $s_group_data->description));
?></div>
              <div><a class="register-link static" href="<?php
      // "print getSessionRegistrationLink($s_group_data->sessionKey);".
      // TODO: use l()
      print t('!url/!subdomain/e.php?AT=SINF&MK=!key', array(
        '!url' => $variables['webex_aggregate']['url'],
        '!subdomain' => $variables['webex_aggregate']['subdomain'],
        '!key' => $s_group_data->sessionKey,
      ));
?>" target="_blank">Register</a></div>
            </li>
            <?php
    endforeach;
?></ul>
        <?php
  endforeach;
endforeach;
?>
