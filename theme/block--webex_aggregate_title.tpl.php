<?php
/**
 * @file
 * Template.
 */

// For Themers:
// var_dump($variables['webex_aggregate']['data']);
foreach ($variables['webex_aggregate']['data'] as $s_category_k => $s_category) :
  if (!empty($s_category_k)) :
?>
      <h3><?php
    print t('!title', array('!title' => $s_category_k));
?></h3>
    <?php
  endif;
?>
  <table class="webex-list">
    <caption>WebEx event registration table</caption>
    <tr><th>Session Title</th><th>Description</th><th>Date</th></tr>
    <?php
  foreach ($s_category as $s_group_a_k => $s_group_a) :
?>
    <tr>
      <td class="title"><?php
    print t('!title', array('!title' => $s_group_a_k));
?></td>
      <?php
    $date_select = ' <select name="webex-event-date" >
      <option>Choose</option>';
    foreach ($s_group_a as $s_group_b_k => $s_group_data) :
      $date_select .= '<option value="' . $s_group_data->sessionKey . '">' . date('m/d/Y g:ia', strtotime($s_group_data->startDate)) . '</option>';
      // Yes we're overwriting this description.
      $group_description = $s_group_data->description;
    endforeach;
    $date_select .= '<select>';
?>
      <td class="description"><?php
    print t('!desc', array('!desc' => $group_description));
?></td>
      <td class="date register">
        <?php
    print t('!date', array('!date' => $date_select));
?>
        <a class="register-link static" href="">Register</a>
      </td>
    </tr>
    <?php
  endforeach;
?>
  </table>
  <?php
endforeach;
