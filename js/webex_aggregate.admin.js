/*
 * @file
 * So far, this file handles the removal of aggregates from the db
 */

//Drupal.t('Comments to @type posts', {'@type': typeName});
jQuery(document).ready(function () {
  var webexDelete = jQuery('.webex_delete');
  webexDelete.click(function (e) {
    e.preventDefault();
    var activeWebex = jQuery(this);
    var confirmDelete = confirm(Drupal.t('Are you sure you want to delete this list?'));
    var webex_aid = activeWebex.attr('rel');
    if (confirmDelete == true) {
      $.ajax({
        type: "POST",
        url: '/admin/config/webex_aggregate/delete/' + webex_aid,
        dataType: 'json',
        success: function (data) {
          if (data.status != 'error') {
            window.location = '/admin/config/webex_aggregate/list/';
          } else {
            alert(Drupal.t('There was an error deleting this record.'));
          }
        }
      });
    }
    return false;
  });
});
