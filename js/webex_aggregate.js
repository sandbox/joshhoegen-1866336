/*
 * This file handles the register links dropdown menu. It pulls the registration id from the appropriate session.
 * Any other js additions specific to this module are welcome here.
 *
 * @todo: url = 'Drupal.settings-js-in-hook_block_view()';
 */
jQuery(document).ready(function () {
  var regLink = jQuery('.webex-list').find('.register-link');
  var url = Drupal.settings.webex_aggregate.url;
  var subdomain = Drupal.settings.webex_aggregate.subdomain;
  regLink.removeAttr('href');
  regLink.attr('target', '_blank').click(function () {
    var eventVal = jQuery(this).attr('href');
    if (!eventVal || eventVal == '' || eventVal == 'Choose') {
      alert(Drupal.t('Please choose a date for this event.'));
      return false;
    }
  });
  jQuery('select[name=webex-event-date]').change(function () {
    var eventLink = jQuery(this);
    var eventVal = eventLink.val();
    var registerLink = eventLink.next('.register-link');
    // blank value returning Choose
    if (!eventVal || eventVal == '' || eventVal == 'Choose') {
      registerLink.removeAttr('href');
    } else {
      registerLink.attr('href', url + '/' + subdomain + '/e.php?AT=SINF&MK=' + eventVal);
    }
  });
});
