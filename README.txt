WebEx API aggregate for Drupal by 
College Board / Inspiring Minds

@author Joshua E Hoegen
 ----------------------------------
 - Aggregate events from your WebEx account via WebEx API 
   http://developer.cisco.com/web/webex-developer/xml-api-overview
 - Checks timestamp in DB to determine whether to grab updated xml from WebEx
 - Add multiple aggregates, display as Drupal blocks
 - has debug mode to see POST and result
 - Option to save local xml cached copies 
 - Includes the begining of a standalone "WebexAPI.class.php" project on GitHub
 - Compatible with Drupal's Encrypt module http://drupal.org/project/encrypt 
 
Usage
 ----------------------------------
 1. Enable Module
 2. Go to admin/config/webex_aggregate/list/ 
 3. Click "Add New WebEx List"
 3. Enter WebEx credentials & save
 4. Add WebEx block to a page
 
TODO:
 ----------------------------------
 - MANAGE OTHER THINGS BESIDES EVENT! (handle, meetings, etc).
   Pretty sure 'meetings' works.
 - Build upon standalone API (include/WebexAPI.class.php) 
   add sorting, cache, etc
 - Default UI (pager, collapsable)
 - New Form Fields/DB Schema: Title field, "Other" replacement (
   see: _api_sort_response), Description 
   Should we tie this to a node? That is, 1 node per whole feed?
 - Form: Make WebEx Sort Sections drag n drop
 - Form validation js & php. specifically "title delimited" and "category sort"
   Hide category sort by default.
