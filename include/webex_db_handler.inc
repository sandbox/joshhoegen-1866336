<?php
/**
 * @file
 * Handles interactions with WebEx table in database.
 * 
 * College Board
 * Inspiring Minds
 *  
 * @todo All db queries happen here, except hook_block_list().
 *   Utilize: webex_aggregate_db_select_webex_lists().
 */

/**
 * DEPRECATED - uses webex_aggregate_db_select_webex_info($aid, TRUE).
 *
 * @param string/int $webex_aid
 *   WebEx aggregate id.
 * 
 * @return string
 *   Stringified json object.
 */
function webex_aggregate_db_select_json($webex_aid) {
  $debug = variable_get('webex_debug', FALSE);
  $args = array(
    'webex_aid' => $webex_aid,
  );
  $sql = "SELECT webex_json, webex_aid, webex_date
    FROM {webex_aggregate}
    WHERE webex_aid = :webex_aid";
  $result = db_query($sql, $args)->fetchObject();
  // Get new WebEx XML result if DB timestamp is les than midnight.
  // This should not be here.
  // Thought it was more appropriate than output.inc...
  // Should actually hook into Drupal's cron?
  if (strtotime('midnight') > $result->webex_date) {
    $update = _api_build_post_xml(array(
      'webex_aid' => $result->webex_aid,
    ));
    if ($update) {
      $result->webex_json = json_encode($update);
    }
  }
  $return = isset($result->webex_json) ? $result->webex_json : FALSE;
  if ($debug) {
    drupal_set_message(t('WebEx JSON: @json', array('@json' => check_plain($return))));
  }
  return $return;
}
/**
 * Get all fields of specified WebEx record.
 *
 * @param string/int $webex_aid
 *   WebEx aggregate id.
 * 
 * @return object
 *   Database object.
 */
function webex_aggregate_db_select_info($webex_aid, $check_json = FALSE) {
  $debug = variable_get('webex_debug', FALSE);
  $args = array(
    ':webex_aid' => $webex_aid,
  );
  $sql = "SELECT * 
    FROM {webex_aggregate}   
    WHERE webex_aid = :webex_aid";
  $result = db_query($sql, $args)->fetchObject();
  if ($check_json) {
    // Get new WebEx XML result if DB timestamp is les than midnight.
    // This should not be here.
    // thought it was more appropriate than output.inc... hmmm
    // this should actually hook into Drupal's cron.
    if (strtotime('midnight') > $result->webex_date) {
      $update = webex_aggregate_api_build_post_xml(array(
        'webex_aid' => $result->webex_aid,
      ));
      if ($update) {
        $result->webex_json = json_encode($update);
      }
    }
    $return = isset($result->webex_json) ? $result->webex_json : FALSE;
    if ($debug) {
      drupal_set_message(t('WebEx JSON: @json', array('@json' => check_plain($return))));
    }
  }
  return $result;
}
/**
 * For use when webex_output.inc is fully removed.
 * 
 * @return array
 *   Database object.
 */
function webex_aggregate_db_select_lists() {
  $result = db_query("SELECT webex_aid, webex_keyword FROM {webex_aggregate};");
  $items = array();
  $list = '';
  foreach ($result as $data) {
    $items[$data->webex_aid] = ($data->webex_keyword != '' ? $data->webex_keyword . '_' : '') . '' . $data->webex_aid;
  }
  return $items;
}
/**
 * If Encrypt contrib, run password through encrypt()/decrypt().
 * 
 * Related: webex_aggregate_edit(), webex_aggregate_db_preprocess_update().
 *  
 * @param string $password
 *   Password for encryption.
 *   
 * @param string $direction
 *   Encryption direction.
 *
 * @return string
 *   Encrypted/decrypted password.
 * 
 * @todo determine final security measures.
 */
function webex_aggregate_db_pw($password = FALSE, $direction = 'encrypt') {
  $webex_password = $password;
  if (module_exists('encrypt')) {
    if ($direction == 'decrypt') {
      $webex_password = decrypt($password);
    }
    else {
      // Getting some crazy encoding issues without utf8_encode().
      $webex_password = encrypt(utf8_encode($password));
    }
  }
  return $webex_password;
}
/**
 * Preprocesses update.
 * 
 * DB/MySQL Timestamps are unreliable/inconvenient in Drupal.
 * We need to cleanse the password and may want to do other processes
 * before DB update.
 *
 * @param array $webex_db_item
 *   Webex db fields.
 * 
 * @return array
 *   Updated array for insert.
 */
function webex_aggregate_db_preprocess_update($webex_db_item = FALSE) {
  $updatewebex_aggregate_db_item = array();
  $updatewebex_aggregate_db_item['webex_date'] = strtotime('now');
  if (!empty($webex_db_item['webex_password'])) {
    $updatewebex_aggregate_db_item['webex_password'] = webex_aggregate_db_pw($webex_db_item['webex_password'], 'encrypt');
  }
  return $updatewebex_aggregate_db_item;
}
/**
 * Insert/Update WebEx item.
 *
 * @param array $webex_db_item
 *   Fields to insert/update.
 * 	 All items are fields in the webex db table.
 * 
 * @return array
 *   DB result.
 */
function webex_aggregate_db_update($webex_db_item) {
  $webex_aid = isset($webex_db_item['webex_aid']) ? $webex_db_item['webex_aid'] : FALSE;
  $update = webex_aggregate_db_preprocess_update($webex_db_item);
  $webex_db_item = array_merge($webex_db_item, $update);
  $args = array(
    ':webex_aid' => $webex_aid,
  );
  $result = db_update('webex_aggregate')->fields($webex_db_item)->condition('webex_aid', $webex_aid, '=')->execute();
  return $result;
}
/**
 * Insert/Update WebEx item.
 *
 * @param array $webex_db_item
 *   Fields to insert/update. 
 * 	 All items are fields in the webex db table.
 * 
 * @return array
 *   DB result.
 */
function webex_aggregate_db_insert($webex_db_item) {
  $update = webex_aggregate_db_preprocess_update($webex_db_item);
  $webex_db_item = array_merge($webex_db_item, $update);
  $result = db_insert('webex_aggregate')->fields($webex_db_item)->execute();
  return $result ? $result : FALSE;
}
/**
 * Insert/Update WebEx item.
 *
 * @param string/int $webex_aid
 *   Webex aggregate id.
 * @param string $webex_json
 *   JSON string.
 * 
 * @return array
 *   DB result.
 */
function webex_aggregate_db_update_json($webex_aid, $webex_json) {
  $debug = variable_get('webex_debug', FALSE);
  $update = webex_aggregate_db_preprocess_update();
  $args = array(
    ':webex_aid' => $webex_aid,
  );
  $fields = array(
    'webex_json' => $webex_json,
    'webex_date' => $update['webex_date'],
  );
  $result = db_update('webex_aggregate')->fields($fields)->condition('webex_aid', $webex_aid, '=')->execute();
  if ($result) {
    drupal_set_message(t('WebEx JSON updated.'));
  }
  else {
    drupal_set_message(t('There was a problem updating JSON.'));
  }
  if ($debug) {
    drupal_set_message(t('webex_aggregate_db_update_json() json: @json', array('@json' => check_plain($webex_json))));
  }
  return $result;
}
/**
 * Insert/Update WebEx item sort order.
 * 
 * Only used if delimited group feature enabled.
 *
 * @param string/int $webex_aid
 *   Webex aggregate id.
 * @param string $webex_sort_order
 *   Comma delimited string.
 * 
 * @return array
 *   DB result.
 */
function webex_aggregate_db_update_sort_order($webex_aid, $webex_sort_order) {
  $args = array(
    ':webex_aid' => $webex_aid,
  );
  $fields = array(
    'webex_sort_sections' => $webex_sort_order,
  );
  $result = db_update('webex_aggregate')->fields($fields)->condition('webex_aid', $webex_aid, '=')->execute();
  return $result;
}
/**
 * Delete WebEx item.
 *
 * @param array $webex_aid
 *   Aggregate ID to delete.
 * 
 * @return array
 *   DB result.
 */
function webex_aggregate_db_delete_webex($webex_aid) {
  $args = array(
    ':webex_aid' => $webex_aid,
  );
  $fields = array(
    'webex_sort_sections' => $webex_sort_order,
  );
  $result = db_delete('webex_aggregate')->condition('webex_aid', $webex_aid);
  return $result;
}
