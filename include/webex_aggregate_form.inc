<?php
/**
 * @file
 * Handles the create new/edit feed form.
 * 
 * College Board
 * Inspiring Minds
 */

/**
 * Drupal form api. Create/edit a feed block.
 * 
 * @param string $form
 *   "api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7"
 * @param array $form_state
 *   Reference.
 *    
 * @return array
 *   Webex array.
 */
function webex_aggregate_edit($form, &$form_state) {
  $debug = variable_get('webex_debug', FALSE);
  $webex_aid = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : FALSE;
  $webex_sort_sections = '';
  $form = array();
  if ($webex_aid) {
    $webex_info = webex_aggregate_db_select_info($webex_aid);
    // $webex_info = array() $webex_info;
    if (!$webex_info) {
      drupal_set_message(t('There was an issue accessing the WebEx aggregate.'));
      return FALSE;
    }
    foreach (json_decode($webex_info->webex_json) as $type_key => $type_val) {
      $webex_sort_sections .= '' . $type_key . ',';
    }
    $webex_sort_sections = substr($webex_sort_sections, 0, -1);
  }
  $form['webex_on'] = array(
    '#type' => 'hidden',
    '#title' => t('Turn WebEx on?'),
    '#description' => '',
    '#options' => array(
      0 => t('Off'),
      1 => t('On'),
      2 => t('Debug Mode'),
    ),
    '#default_value' => 1 ,
  );
  $form['webex_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('WebEx Keyword'),
    '#default_value' => !empty($webex_info->webex_keyword) ? $webex_info->webex_keyword : '',
  );
  $form['webex_user'] = array(
    '#type' => 'textfield',
    '#title' => t('WebEx Username'),
    '#required' => TRUE,
    '#default_value' => !empty($webex_info->webex_user) ? $webex_info->webex_user : '',
  );
  $form['webex_password'] = array(
    '#type' => 'textfield',
    '#title' => t('WebEx Password'),
    '#description' => t('Password required to make changes to this feed.'),
    '#required' => TRUE,
    '#default_value' => '',
    // For Debugging:
    // '#prefix' => $debug ?
    // 'Decrypted PW:
    // ' . webex_aggregate_db_pw($webex_info->webex_password, 'decrypt') : '',
  );
  $form['webex_sid'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID (SID)'),
    '#required' => TRUE,
    '#default_value' => !empty($webex_info->webex_sid) ? $webex_info->webex_sid : '',
  );
  $form['webex_pid'] = array(
    '#type' => 'textfield',
    '#title' => t('Partner ID (PID)'),
    '#required' => TRUE,
    '#default_value' => !empty($webex_info->webex_pid) ? $webex_info->webex_pid : '',
  );
  $form['webex_url'] = array(
    '#type' => 'textfield',
    '#description' => t('Please enter the subdomain prefix. Ex: http://subdomainprefix.webex.com'),
    '#title' => t('WebEx URL'),
    '#required' => TRUE,
    '#default_value' => !empty($webex_info->webex_url) ? $webex_info->webex_url : '',
  );
  // @todo Logic to build post XML. See: _api_build_post_xml.
  // Will need more options than just event & meeting.
  $form['webex_list_type'] = array(
    '#type' => 'select',
    '#title' => t('WebEx List Type'),
    '#description' => t('Events, meetings, etc.'),
    '#required' => TRUE,
    '#options' => array(
      'event' => t('Event'),
      'meeting' => t('Meeting'),
    ),
    '#default_value' => !empty($webex_info->webex_list_type) ? $webex_info->webex_list_type : '',
  );
  // Let's make this a drill down filter
  // [] CONFNAME () ASC () DESC
  // [] STARTTIME () ASC () DESC
  // It will fix the fun in _api_build_post_xml()!
  $form['webex_sort'] = array(
    '#type' => 'select',
    '#title' => t('Sort By'),
    '#description' => t('The order of these sort parameters will group the output with the same precedence.'),
    // This odd/even will play a roll here:
    '#options' => array(
      0 => t('TITLE ASC > STARTTIME'),
      1 => t('STARTTIME ASC > TITLE'),
      2 => t('TITLE ASC > STARTTIME DESC'),
      3 => t('STARTTIME DESC > TITLE'),
      4 => t('TITLE DESC > STARTTIME'),
    ),
    '#default_value' => !empty($webex_info->webex_sort) ? $webex_info->webex_sort : '',
  );
  /*$form['webex_group_by'] = array(
  '#type' => 'select',
  '#title' => t('WebEx Sort'),
  '#description' => '',
  // this odd/even will play a roll here:
  '#options' => array(
  0 => t('Title'),
  1 => t('Date'),
  2 => t('Do Not Group'),
  ),
  '#default_value' => !empty($webex_info->webex_sort) ?
  $webex_info->webex_sort : ''
  );*/
  // @todo Timezone is an int.
  // Must figure out the corresponding timezones.
  // Currently under the assumption that timezone '11' is EST.
  /*$form['webex_time_zone'] = array(
  '#type' => 'select',
  '#title' => t('Time Zone'),
  '#required' => TRUE,
  '#options' => array(
  1 => t('EST'),
  2 => t('CST'),
  //etc...
  ),
  );*/
  $form['webex_max_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Time Span ("+3 months", "+1 week")'),
    '#description' => 'The value of this field will be ran through strtotime().',
    '#required' => TRUE,
    '#default_value' => !empty($webex_info->webex_max_date) ? $webex_info->webex_max_date : '3 months',
  );
  $form['webex_max_record'] = array(
    '#type' => 'select',
    '#title' => t('How many records do you want returned?'),
    '#required' => TRUE,
    '#options' => array(
      10 => t('10'),
      20 => t('20'),
      50 => t('50'),
      70 => t('70'),
      100 => t('100'),
    ),
    '#default_value' => !empty($webex_info->webex_max_record) ? $webex_info->webex_max_record : 50,
  );
  $form['webex_title_delimiter'] = array(
    '#type' => 'textfield',
    '#title' => t('Title Delimiter'),
    '#description' => t('When using a delimiter, a new table will be created for each "Section", when labeled as such "Program | Section | Event Title". If no value, the output will be displayed in a single table when grouped by title. The second to last item will always be the "Section" title.'),
    '#default_value' => !empty($webex_info->webex_title_delimiter) ? $webex_info->webex_title_delimiter : '',
  );
  // Make this Drag n Drop.
  $form['webex_sort_sections'] = array(
    '#type' => 'textarea',
    '#title' => t('WebEx Order Categories'),
    '#description' => t('If your titles are delimited, you can rearrange the comma delimited list for the desired output. 
      To set the default order, leave blank.'),
    '#default_value' => !empty($webex_info->webex_sort_sections) ? $webex_info->webex_sort_sections : $webex_sort_sections,
  );
  $form['webex_aid'] = array(
    '#type' => 'hidden',
    '#value' => !empty($webex_info->webex_aid) ? $webex_info->webex_aid : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array(
      'webex_aggregate_edit_submit',
    ),
  );
  return $form;
}
/**
 * Drupal form api. Submits form.
 * 
 * @param string $form
 *   "api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7"
 * @param array $form_state
 *   Reference.
 */
function webex_aggregate_edit_submit($form, &$form_state) {
  $webex_db_item = array();
  // We want to only include fields that are in the db.
  $webex_db_item['webex_on'] = $form['webex_on']['#value'];
  $webex_db_item['webex_keyword'] = $form['webex_keyword']['#value'];
  $webex_db_item['webex_user'] = $form['webex_user']['#value'];
  $webex_db_item['webex_password'] = $form['webex_password']['#value'];
  $webex_db_item['webex_sid'] = $form['webex_sid']['#value'];
  $webex_db_item['webex_pid'] = $form['webex_pid']['#value'];
  $webex_db_item['webex_url'] = $form['webex_url']['#value'];
  $webex_db_item['webex_list_type'] = $form['webex_list_type']['#value'];
  $webex_db_item['webex_max_date'] = $form['webex_max_date']['#value'];
  $webex_db_item['webex_max_record'] = $form['webex_max_record']['#value'];
  $webex_db_item['webex_sort'] = $form['webex_sort']['#value'];
  $webex_db_item['webex_group_by'] = $webex_db_item['webex_sort'] % 2 == 0 ? 'title' : 'date';
  $webex_db_item['webex_title_delimiter'] = $form['webex_title_delimiter']['#value'];
  $webex_db_item['webex_sort_sections'] = $form['webex_sort_sections']['#value'];
  if (!empty($form['webex_aid']['#value'])) {
    $webex_db_item['webex_aid'] = $form['webex_aid']['#value'];
    $result = webex_aggregate_db_update($webex_db_item);
    webex_aggregate_api_build_post_xml($webex_db_item);
  }
  else {
    $result = webex_aggregate_db_insert($webex_db_item);
    $webex_db_item['webex_aid'] = $result;
    webex_aggregate_api_build_post_xml($webex_db_item);
    $form_state['redirect'] = array(
      url('admin/config/webex_aggregate/edit/' . $result, array('alias' => TRUE)),
    );
  }
}
