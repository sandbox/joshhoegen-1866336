<?php
/**
 * @file
 * Handles theme preprocesses.
 * 
 * College Board
 * Inspiring Minds
 * 
 * More or less the "themed output". used in webex_aggregate_block_view().
 * The contents of this file should be moved to webex_aggregate.module.
 * 
 * @todo Move webex_aggregate_output() and remove file.
 */

/**
 * DEPRECATED! Aiming to use renderable arrays theme_list().
 *
 * @param string/int $webex_aid
 *   Webex ID.
 * 
 * @return object
 *   Output object for theming.
 * 
 * @todo put logic in theme.
 */
function webex_aggregate_output($webex_aid) {
  $debug = variable_get('webex_debug', FALSE);
  $webex_info = webex_aggregate_db_select_info($webex_aid, TRUE);
  $webex_info->webex_json = json_decode($webex_info->webex_json);
  $parsed_url = parse_url($webex_info->webex_url);
  $host = explode('.', $parsed_url['host']);
  $webex_subdomain = $host[0];
  $sessions = array(
    'aid' => $webex_info->webex_aid,
    'data' => $webex_info->webex_json,
    'sort_by' => $webex_info->webex_sort,
    'url' => $webex_info->webex_url,
    'subdomain' => $webex_subdomain,
  );
  if (!$sessions) {
    drupal_set_message(t('Problem Parsing WebEx JSON. Please contact the site administrator.'));
    $sessions = FALSE;
  }
  return $sessions;
}
/**
 * DEPRECATED! Aiming to use renderable arrays theme_list().
 *
 * @param object $data
 *   To loop through.
 * @param string $title
 *   Html - tag - h3, span, etc.
 * @param string $parent
 *   Html - ul, div.
 * @param string $child
 *   Html - li, div.
 * 
 * @return string
 *   Html.
 * 
 * @todo Table logic.
 */
function webex_aggregate_bundle_output($data, $title, $parent, $child) {
  $output = '';
  foreach ($data as $k => $v) {
    $output .= '<' . $parent . '>';
    if (is_object($v) || is_array($v)) {
      $output .= '<' . $child . '><' . $title . '>' . $k . '</' . $title . '>';
      $output .= bundle_output($v, $title, $parent, $child);
      $output .= '</' . $child . '>';
    }
    else {
      $output .= '' . $v . '';
    }
    $output .= '</' . $parent . '>';
  }
  return $output;
}
