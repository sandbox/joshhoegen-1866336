<?php
/**
 * @file
 * Handles POSTS to the WebEx API.
 * 
 * College Board
 * Inspiring Minds
 * 
 * @todo Ensure (deprecated) cached XML parsing still works.
 */

/**
 * Builds an xml for post to WebEx service.
 *
 * @param array/object $webex_db_item
 *   Required and optional webex db fields.
 * 
 * @return bool 
 *   Whether XML build & _api_send_post_xml was succesful.  
 */
function webex_aggregate_api_build_post_xml($webex_db_item) {
  $success = FALSE;
  $debug = variable_get('webex_debug', FALSE);
  if (!isset($webex_db_item['webex_password'])) {
    $webex_db_item = (array) webex_aggregate_db_select_info($webex_db_item['webex_aid']);
    $webex_db_item['webex_password'] = webex_aggregate_db_pw($webex_db_item['webex_password'], 'decrypt');
  }
  if (!$webex_db_item) {
    return FALSE;
  }
  $order_event_conf = $webex_db_item['webex_list_type'] == 'event' ? 'EVENTNAME' : 'CONFNAME';
  $ob1 = $order_event_conf;
  $oad1 = 'ASC';
  $ob2 = 'STARTTIME';
  $oad2 = 'ASC';
  // Odd / even.
  switch ($webex_db_item['webex_sort']) {
    case 0:
      $ob1 = $order_event_conf;
      $oad1 = 'ASC';
      $ob2 = 'STARTTIME';
      $oad2 = 'ASC';
      break;

    case 1:
      $ob1 = $order_event_conf;
      $oad1 = 'DESC';
      $ob2 = 'STARTTIME';
      $oad2 = 'ASC';
      break;

    case 2:
      $ob1 = $order_event_conf;
      $oad1 = 'DESC';
      $ob2 = 'STARTTIME';
      $oad2 = 'DESC';
      break;

    case 3:
      $ob1 = 'STARTTIME';
      $oad1 = 'ASC';
      $ob2 = $order_event_conf;
      $oad2 = 'ASC';
      break;

    case 4:
      $ob1 = 'STARTTIME';
      $oad1 = 'DESC';
      $ob2 = $order_event_conf;
      $oad2 = 'ASC';
      break;

  }
  $record_order_by = '<orderBy>' . $ob1 . '</orderBy>
	<orderAD>' . $oad1 . '</orderAD>
	<orderBy>' . $ob2 . '</orderBy>
	<orderAD>' . $oad2 . '</orderAD>';
  $webex_db_item['webex_sort'] = $record_order_by;
  // Calling our WebexAPI.class
  $webex_list = new WebexAPI($webex_db_item);
  $events = $webex_list->getWebex();
  $create_local_xml = variable_get('webex_cache_xml', FALSE);
  if ($create_local_xml) {
    webex_aggregate_save_local_xml($webex_db_item['webex_aid'], $webex_list);
  }
  $sessions = webex_aggregate_api_sort_response($events, $webex_db_item);
  $db_cache = json_encode($sessions);
  webex_aggregate_db_update_json($webex_db_item['webex_aid'], $db_cache);
  if ($webex_db_item['webex_sort_sections'] == '') {
    $sort_order_sections = '';
    foreach (array_keys($sessions) as $session_section) {
      $sort_order_sections .= $session_section . ',';
    }
    $sort_order_sections = substr($sort_order_sections, 0, -1);
    webex_aggregate_db_update_sort_order($webex_db_item['webex_aid'], $sort_order_sections);
  }
  $success = $sessions;
  if ($debug) {
    drupal_set_message('@output', array(
      '@output' => $webex_list->getErrorMsg(),
    ));
  }
  return $success;
}
/**
 * DEPRECATED? Can be enabled in the global settings form. 
 * 
 * Saves the returned XML.
 *
 * @param string/int $webex_aid
 *   Id of webex aggregate.
 * @param string $webex_list
 *   List type.
 *   
 * @return bool
 *   Whether XML build & _api_send_post_xml was succesful. 
 */
function webex_aggregate_save_local_xml($webex_aid, $webex_list) {
  $webex_list_type = $webex_list->webex_list_type;
  // Prepare xml cache.
  $file = webex_aggregate_determine_file_location($webex_list_type . '_' . ($webex_aid ? $webex_aid : '') . '.xml');
  if (file_prepare_directory(dirname($file), FILE_CREATE_DIRECTORY)) {
    if (file_exists($file)) {
      domxml_unlink_node($file);
    }
    $fp = fopen($file, 'w');
    if (fwrite($fp, $webex_list->webex_response)) {
      if (user_is_logged_in()) {
        fclose($fp);
        drupal_set_message(t('New WebEx XML file created. @file', array(
          '@file' => $file,
        )));
      }
    }
    else {
      $return = FALSE;
      if (user_is_logged_in()) {
        drupal_set_message(t('Error in WebEx XML file creation.'));
      }
    }
  }
}
/**
 * The uksort() couldn't pick up what i was tring to put down.
 * 
 * @param array $array
 *   Raw array to be ordered.
 * @param array $order_array
 *   Order of keys.
 *   
 * @return array
 *   Sorted array.
 */
function webex_aggregate_sort_array_by_array($array, $order_array) {
  $ordered = array();
  foreach ($order_array as $key) {
    if (array_key_exists($key, $array)) {
      $ordered[$key] = $array[$key];
      unset($array[$key]);
    }
  }
  return $ordered + $array;
}
/**
 * Sort response.
 *
 * @param array/object $events
 *   $xml_obj->children - from build_post_xml().
 * @param array $webex_db_item
 *   Params from webex aggregate table needed to sort the 
 *   returned xml in a way that is too detailed for the webex api.
 * 
 * @return array
 *   New order and structure to store as json in the DB.
 * 
 * @todo is there a better way to do this? In my experience, 
 *   I found x-path to be slow in parsing the xml.
 */
function webex_aggregate_api_sort_response($events, $webex_db_item) {
  $sessions = array();
  $output_keyword = $webex_db_item['webex_keyword'];
  $sort_index = $webex_db_item['webex_sort'];
  $group_by = $webex_db_item['webex_group_by'];
  $sort_order = explode(',', $webex_db_item['webex_sort_sections']);
  $session_title_delimiter = $webex_db_item['webex_title_delimiter'];
  // Lets sort and group this xml.
  // I've been racking my brain for too many hours on the.
  // Best way to do this (had nested loops with conditionals, etc).
  // Would love to hear a best practice.
  foreach ($events as $e_key => $e_obj) {
    $session_key = (string) $e_obj->sessionKey;
    $session_name = (string) $e_obj->sessionName;
    $session_description = (string) $e_obj->description;
    $session_start_date = (string) $e_obj->startDate;
    $keyword_status = TRUE;
    // If the comma delimited feature is enabled.
    $session_name_array = explode(' ' . trim($session_title_delimiter) . ' ', $session_name);
    // If your client isn't storing data neatly...
    // "Division X" & "Division Y" are stored with the same program ID,
    // And the divisions.
    // Want to create subgroups intro, overview, advanced.
    // For now, we are going to have to name our sessions like this:
    // "Division X | Introductory | Some title goes here" or "Advanced - Title".
    $name_count = count($session_name_array);
    if ($name_count == 1 || empty($session_title_delimiter)) {
      $session_category = 0;
      $session_title = $session_name_array[0];
    }
    elseif ($name_count == 2) {
      $session_category = $session_name_array[0];
      $session_title = $session_name_array[1];
    }
    elseif ($name_count >= 3) {
      $session_category = $session_name_array[$name_count - 2];
      $session_title = $session_name_array[$name_count - 1];
    }
    // Because multiple programs were/are using the same webex program id.
    // If keyword match or null
    // preg_match('/' . $output_keyword . '|\0/i', $session_group_b).
    $keyword_status = $output_keyword != '' ? preg_match('/' . $output_keyword . '/i', $session_group_b) : TRUE;
    $session_group_a = 0;
    $session_group_b = $session_key;
    if ($group_by == 'title') {
      // UNIQUE TO OUR NAMING CONVENTION.
      $session_group_a = $session_title;
      $session_group_b = date('Y-m-d', strtotime($session_start_date));
    }
    else {
      // Shifting the $session_category_group to $session_group_a,
      // Allowing date to move in.
      $session_group_a = date('Y-m-d', strtotime($session_start_date));
      $session_group_b = $session_title;
    }
    if ($keyword_status) {
      // We're setting the array up for it's easy output.
      // Should probably make this an object.
      $e_obj->sessionNameOutput = $session_title;
      $sessions[$session_category][$session_group_a][$session_group_b] = $e_obj;
      // $sessions[$session_category][$session_key] = $e_obj;
    }
  }
  $sessions = webex_aggregate_sort_array_by_array($sessions, $sort_order);
  return $sessions;
}
/**
 * Check to see if an alternate file path is saved.
 * 
 * This function checks to see if a different 
 * directory other than the default 
 * "Public file system path - (admin/config/media/file-system)" 
 * has been configured to store the local copy of the XML. 
 *
 * @param string $file_name
 *   XML file.
 * 
 * @return string
 *   The dirname().
 * 
 * @todo Currently, setting variable_get('webex_alternate_file_path')
 *   is not available. When adding the alt file path, add it here: 
 *   _display_global_settings_form() in list.inc
 */
function webex_aggregate_determine_file_location($file_name) {
  $file = FALSE;
  $alternate_file_path = variable_get('webex_alternate_file_path', variable_get('file_public_path', conf_path() . '/files'));
  if ($alternate_file_path == '') {
    $alternate_file_path = variable_get('file_public_path', conf_path() . '/files');
  }
  $file = dirname($_SERVER['SCRIPT_FILENAME']) . '/' . $alternate_file_path . '/webex_xml_cache/' . $file_name;
  if (!file_prepare_directory($alternate_file_path, FILE_CREATE_DIRECTORY)) {
    // If all paths fail try this folder.
    $file = dirname($_SERVER['SCRIPT_FILENAME']) . '/' . $alternate_file_path . '/' . $file_name;
  }
  return $file;
}
/**
 * DEPRECATED - uses resorted json in DB.
 * 
 * This function opens and reads the xml specified in param.
 *
 * @param string $xml_path
 *   Path to xml file.
 * 
 * @return string
 *   Contents of xml file.
 */
function webex_aggregate_read_xml_from_cache($xml_path) {
  $xml_file = fopen($xml_path, 'r');
  $xml_date = filemtime($xml_file);
  $xml_date_midnight = strtotime('midnight today');
  if (!$xml_date) {
    $xml_date = get_headers('http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace($_SERVER['DOCUMENT_ROOT'], '', $xml_path), 1);
    $xml_date = !strtotime($xml_date['Last-Modified']) ? strtotime($xml_date['Date']) : strtotime($xml_date['Last-Modified']);
  }
  if (!$xml_file) {
    webex_aggregate_api_build_post_xml();
    $xml_file = fopen($xml_path, 'r');
    if (user_is_logged_in()) {
      drupal_set_message(t('NEW WebEx XML file created. @file', array(
        '@file' => $file,
      )));
    }
  }
  elseif ($xml_date_midnight > $xml_date) {
    webex_aggregate_api_build_post_xml();
    $xml_file = fopen($xml_path, 'r');
    if (user_is_logged_in()) {
      drupal_set_message(t("TODAY's WebEx XML file created. @file", array(
        '@file' => $file,
      )));
    }
  }
  $contents = '';
  while (!feof($xml_file)) {
    $contents .= fread($xml_file, 8192);
  }
  fclose($xml_file);
  return $contents;
}
/**
 * DEPRECATED - uses resorted json in DB.
 * 
 * This function opens and reads the xml specified in param.
 *
 * @param string/int $webex_aid
 *   webex aggregate id
 * @param string $xml_path
 *   path to xml file
 * 
 * @return string
 *   contents of xml file
 */
function webex_aggregate_consume_xml_from_cache($webex_aid, $xml_path) {
  $contents = webex_aggregate_read_xml_from_cache($xml_path);
  // Remove these. Ensure no other dependencies.
  $xml_obj = new SimpleXMLElement($contents);
  $events = $xml_obj->children('serv', TRUE)->body->bodyContent->children('event', TRUE)->event;
  $output = '';
  $output_event = '';
  // Needs second param. Will need to elect params from db handler.
  $sessions = webex_aggregate_api_sort_response($events);
  if (empty($sessions)) {
    return FALSE;
  }
  else {
    return $sessions;
  }
}
