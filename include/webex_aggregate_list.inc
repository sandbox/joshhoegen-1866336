<?php
/**
 * @file
 * Global WebEx settings and a list of saved aggregates.
 * 
 * College Board
 * Inspiring Minds
 * 
 * This file handles the global settings form and 
 * lists available aggregate for editing.
 * 
 * @todo Change module name
 */

/**
 * Displays list of saved aggregate blocks.
 * 
 * @return string
 *   Html string.
 */
function webex_aggregate_display_admin() {
  $lists = webex_aggregate_db_select_lists();
  $output = '<div>
    <a class="webex_add" href="' . url('admin/config/webex_aggregate/edit/', array('alias' => TRUE)) . '">' . t('Add New WebEx Aggregate') . '</a>
    </div>
    <table>';
  $output .= '<tr><th>' . t('List Name') . '</th><th>' . t('Edit List') . '</th><th>' . t('Delete List') . '</th></tr>';
  foreach ($lists as $k => $v) {
    $output .= '<tr>
      <td>' . t('@val', array('@val' => $v)) . '</td>
      <td><a class="webex_edit" href="' . url('admin/config/webex_aggregate/edit/' . $k, array('alias' => TRUE)) . '">' . t('Edit') . '</a></td>
      <td><a class="webex_delete" href="#" rel="' . t('@val', array('@val' => $k)) . '">' . t('Delete') . '</a></td>
      </tr>';
  }
  $output .= '</table>';
  return ($output == '' ? FALSE : $output);
}
/**
 * Implements hook_form().
 */
function webex_aggregate_global_settings_form() {
  drupal_add_js(drupal_get_path('module', 'webex_aggregate') . '/js/webex_aggregate.admin.js');
  $form = array();
  $form['webex_debug'] = array(
    '#type' => 'select',
    '#title' => t('Debug Mode'),
    '#description' => t('Output cURL/XML response, JSON, etc as Drupal messages.'),
    '#options' => array(
      0 => t('Off'),
      1 => t('On'),
    ),
    '#default_value' => variable_get('webex_debug', 0),
  );
  $form['webex_cache_xml'] = array(
    '#type' => 'select',
    '#title' => t('Create XML on Update'),
    '#description' => t('Create a copy of the returned WebEx XML.'),
    '#options' => array(
      0 => t('Off'),
      1 => t('On'),
    ),
    '#default_value' => variable_get('webex_cache_xml', 0),
  );
  $form['webex_alternate_file_path'] = array(
    '#type' => 'textfield',
    '#title' => t('XML File Path'),
    '#description' => t('Path to save WebEx XML files (Default: "sites/default/files").'),
    '#default_value' => variable_get('webex_alternate_file_path', variable_get('file_public_path', conf_path() . '/files')),
  );
  $form['#suffix'] = webex_aggregate_display_admin();
  return system_settings_form($form);
}
